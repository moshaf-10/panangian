// EVENT
(function() {
	headerStick();
	headShow();
	ratioVideo();
	topButton();
	galleryPopup();
	$('.header.sticky').before(`
	<a href="#" id="top-button"></a>
	<div class="wa-us" onclick="window.open('https://api.whatsapp.com/send?phone=628119355488')"><div class="ht_ctc_style ht_ctc_chat_style"><div class="ht_ctc_desktop_chat"><div class="chip ctc-analytics" style="display:flex;justify-content: center;align-items: center;background-color:#e4e4e4;color:#0a0909;padding:0 12px;border-radius:25px;font-size:13px;line-height:32px; "><span style="margin:0 8px 0 -12px;;order:0;"><svg style="pointer-events:none; display: block; height:40px; width:40px;" width="40px" height="40px" viewBox="0 0 1219.547 1225.016"><path fill="#E0E0E0" d="M1041.858 178.02C927.206 63.289 774.753.07 612.325 0 277.617 0 5.232 272.298 5.098 606.991c-.039 106.986 27.915 211.42 81.048 303.476L0 1225.016l321.898-84.406c88.689 48.368 188.547 73.855 290.166 73.896h.258.003c334.654 0 607.08-272.346 607.222-607.023.056-162.208-63.052-314.724-177.689-429.463zm-429.533 933.963h-.197c-90.578-.048-179.402-24.366-256.878-70.339l-18.438-10.93-191.021 50.083 51-186.176-12.013-19.087c-50.525-80.336-77.198-173.175-77.16-268.504.111-278.186 226.507-504.503 504.898-504.503 134.812.056 261.519 52.604 356.814 147.965 95.289 95.36 147.728 222.128 147.688 356.948-.118 278.195-226.522 504.543-504.693 504.543z"></path><linearGradient id="htwaicona-chat-s4" gradientUnits="userSpaceOnUse" x1="609.77" y1="1190.114" x2="609.77" y2="21.084"><stop offset="0" stop-color="#20b038"></stop><stop offset="1" stop-color="#60d66a"></stop></linearGradient><path fill="url(#htwaicona-chat-s4)" d="M27.875 1190.114l82.211-300.18c-50.719-87.852-77.391-187.523-77.359-289.602.133-319.398 260.078-579.25 579.469-579.25 155.016.07 300.508 60.398 409.898 169.891 109.414 109.492 169.633 255.031 169.57 409.812-.133 319.406-260.094 579.281-579.445 579.281-.023 0 .016 0 0 0h-.258c-96.977-.031-192.266-24.375-276.898-70.5l-307.188 80.548z"></path><image overflow="visible" opacity=".08" width="682" height="639" transform="translate(270.984 291.372)"></image><path fill-rule="evenodd" clip-rule="evenodd" fill="#FFF" d="M462.273 349.294c-11.234-24.977-23.062-25.477-33.75-25.914-8.742-.375-18.75-.352-28.742-.352-10 0-26.25 3.758-39.992 18.766-13.75 15.008-52.5 51.289-52.5 125.078 0 73.797 53.75 145.102 61.242 155.117 7.5 10 103.758 166.266 256.203 226.383 126.695 49.961 152.477 40.023 179.977 37.523s88.734-36.273 101.234-71.297c12.5-35.016 12.5-65.031 8.75-71.305-3.75-6.25-13.75-10-28.75-17.5s-88.734-43.789-102.484-48.789-23.75-7.5-33.75 7.516c-10 15-38.727 48.773-47.477 58.773-8.75 10.023-17.5 11.273-32.5 3.773-15-7.523-63.305-23.344-120.609-74.438-44.586-39.75-74.688-88.844-83.438-103.859-8.75-15-.938-23.125 6.586-30.602 6.734-6.719 15-17.508 22.5-26.266 7.484-8.758 9.984-15.008 14.984-25.008 5-10.016 2.5-18.773-1.25-26.273s-32.898-81.67-46.234-111.326z"></path><path fill="#FFF" d="M1036.898 176.091C923.562 62.677 772.859.185 612.297.114 281.43.114 12.172 269.286 12.039 600.137 12 705.896 39.633 809.13 92.156 900.13L7 1211.067l318.203-83.438c87.672 47.812 186.383 73.008 286.836 73.047h.255.003c330.812 0 600.109-269.219 600.25-600.055.055-160.343-62.328-311.108-175.649-424.53zm-424.601 923.242h-.195c-89.539-.047-177.344-24.086-253.93-69.531l-18.227-10.805-188.828 49.508 50.414-184.039-11.875-18.867c-49.945-79.414-76.312-171.188-76.273-265.422.109-274.992 223.906-498.711 499.102-498.711 133.266.055 258.516 52 352.719 146.266 94.195 94.266 146.031 219.578 145.992 352.852-.118 274.999-223.923 498.749-498.899 498.749z"></path></svg></span><span class="ctc_cta">WhatsApp us</span></div></div></div></div>
	`);
})();

$(window).scroll(function (e) {
	headShow();
	topButton();
});

$(window).on('resize', function(){
	fixHeaderPCSP();
	clamp();
});

$(window).on('load', function() {
	clamp();
});
// END EVENT

// COMMON
function headerStick() {
	var headScode = $('header').html();
 $('body').prepend('<header class="header sticky">'+headScode+'</header>');
	
	var logo = $('.header.sticky .logo img').attr('src');
	logoSrc = logo.substr(0, logo.indexOf('.')-3)
	$('.header.sticky .logo img').attr('src', logoSrc+'.png')
}

function headShow() {
	var scrPos = $(window).scrollTop(),
					showPos = $('.main-visual').height();
	if (scrPos > showPos) {
		$('.header.sticky').addClass('on')
	} else {
		$('.header.sticky').removeClass('on')
	}
}
function fixHeaderPCSP() {
	var win = $(window).width();
	if (win > 850) {
		$('body').removeClass('lock')
		$('.gnav').removeAttr("style")
	} else {
		if ($('.burger-menu').hasClass('on')) {
			$('body').addClass('lock')
			$('.burger-menu.on + .gnav').fadeIn(300);
		}
	}
}

function topButton() {
	var scrPos = $(window).scrollTop(),
					showPos = $('.main-visual').height();
	if (scrPos > showPos+(showPos*0.5)) {
		$('#top-button').addClass('on');
	} else {
		$('#top-button').removeClass('on');
	}
}
$('#top-button').on('click', function (e) {
	e.preventDefault();
	$('html, body').animate({
		scrollTop: 0
}, 1000);
});

$('.burger-menu').on('click', function(e){
	if ($('.burger-menu').hasClass('on')) {
		$('body').removeClass('shadow lock')
		$(this).removeClass('on')
		$(this).next('.gnav').fadeOut(500);
	} else {
		if ($('.main-visual').hasClass('half-mv')) {
			$('body').addClass('shadow');
		}
		$('body').addClass('lock')
		$(this).addClass('on')
		$(this).next('.gnav').fadeIn(300);
	}
});

function validateEmail(mail) {
	const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(mail);
}
var slideIndex = 1;
	showSlides(slideIndex);
	function plusSlides(n){showSlides(slideIndex += n);}
	function currentSlide(n){showSlides(slideIndex = n);}
	function showSlides(n) {
			var i;
			var slides = $('.fade-show a');
			var dots = $('.contact-icon *');
			if (n > slides.length) {slideIndex = 1}    
			if (n < 1) {slideIndex = slides.length}
			for (i = 0; i < slides.length; i++) {
							slides[i].style.display = "none";  
			}
			for (i = 0; i < dots.length; i++) {
							dots[i].className = dots[i].style.display = "none";
			}
			slides[slideIndex-1].style.display = "block";  
			dots[slideIndex-1].style.display = "block"; 
	}
	setInterval(function(){plusSlides(1)},5000);
	$('.form-news-letter button[type="submit"]').on('click',function(e){
		e.preventDefault();
		var mail = $('#news-letter-mail').val();
		if (mail == '') {
			Swal.fire({
				icon: 'warning',
				title: 'Oops...',
				text: 'Alamat email belum diisi.',
				showConfirmButton: false,
				timer: 2000
			});
		} else if (!validateEmail(mail)) {
			Swal.fire({
				icon: 'error',
				title: 'Error',
				text: 'Alamat email tidak berlaku.',
				showConfirmButton: false,
				timer: 2000
			});
		}else{
			Swal.fire({
				icon: 'success',
				title: 'Thankyou!',
				text: "Sekarang email anda telah didaftarkan.",
				showConfirmButton: false,
				timer: 2000
			});
			setTimeout(function(){
				$('.form-news-letter').trigger('submit');
			}, 2000);
		}
	});
// END COMMON

// COMPONENT
$('a').click(function(e){
	var url = $(this).attr('href');
	if (url == '#') {
		e.preventDefault();
	}
});

$('.card-click').on('click', function () {
	var link = $(this).find('.card-link').attr('href');
	window.location.href = link;
})

$('.regular-form').change(function(){
	if($(this).val() !== null || $(this).val() !== '') {
		$(this).removeClass('focus');
	}
})

$('#webinar-regist button[type="submit"]').on('click',function(e){
	e.preventDefault();
	$('#webinar-regist .form-group.require .regular-form').removeClass('focus');

	var formInvalid = 0;
	$('#webinar-regist .form-group.require').each( function () {
		var dataFrom = $(this).find('.regular-form').val();
		if(dataFrom === null || dataFrom === '') {
			formInvalid = 5;
			$(this).find('.regular-form').addClass('focus');
		}else{
			
			
			if ($(this).find('.regular-form').attr('name') === 'email' && !validateEmail(dataFrom)) {
				formInvalid = 1;
				$(this).find('.regular-form').addClass('focus');
			}
		}
	});

	if (formInvalid > 1) {
		Swal.fire({
			icon: 'warning',
			title: 'Data belum lengkap',
			text: 'Mohon lengkapi data formulir anda.',
			showConfirmButton: false,
			timer: 2000
		});
	} else if (formInvalid === 1) {
		Swal.fire({
			icon: 'error',
			title: 'Error',
			text: 'Alamat email tidak berlaku.',
			showConfirmButton: false,
			timer: 2000
		});
	} else {
		Swal.fire({
					icon: 'success',
					title: 'Thankyou!',
					text: "Data anda akan segera kami proses.",
					showConfirmButton: false,
					timer: 2000
				});
				setTimeout(function(){
					$('#webinar-regist').trigger('submit');
				}, 2000);
	}
});

$('#contact-us button[type="submit"]').on('click',function(e){
	e.preventDefault();
	$('#contact-us .form-group.require .regular-form').removeClass('focus');

	var formInvalid = 0;
	$('#contact-us .form-group.require').each( function () {
		var dataFrom = $(this).find('.regular-form').val();
		if(dataFrom === null || dataFrom === '') {
			formInvalid = 5;
			$(this).find('.regular-form').addClass('focus');
		}else{
			
			
			if ($(this).find('.regular-form').attr('name') === 'email' && !validateEmail(dataFrom)) {
				formInvalid = 1;
				$(this).find('.regular-form').addClass('focus');
			}
		}
	});

	if (formInvalid > 1) {
		Swal.fire({
			icon: 'warning',
			title: 'Data belum lengkap',
			text: 'Mohon lengkapi data formulir anda.',
			showConfirmButton: false,
			timer: 2000
		});
	} else if (formInvalid === 1) {
		Swal.fire({
			icon: 'error',
			title: 'Error',
			text: 'Alamat email tidak berlaku.',
			showConfirmButton: false,
			timer: 2000
		});
	} else {
		Swal.fire({
					icon: 'success',
					title: 'Thankyou!',
					text: "Data anda akan segera kami proses.",
					showConfirmButton: false,
					timer: 2000
				});
				setTimeout(function(){
					$('#contact-us').trigger('submit');
				}, 2000);
	}
});

$('.accord-button').on('click', function () {
	if ($(this).parent().hasClass('on')) {
		$(this).parent().removeClass('on')
	} else {
		$(this).parent().addClass('on')
	}
})

$('.play-button').on('click', function (e) {
	e.preventDefault();
	var videoUrl = $(this).attr('href');
	if ($(this).hasClass('yt-popup')) {
		$('body').addClass('lock');
		$('.footer').after('<div class="modal-fullscreen"><div class="container"><div class="video-player"><div class="btn-close">&#10006;</div><iframe id="video" width="100%" src="'+videoUrl+'" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div></div></div>');
		$('.modal-fullscreen').fadeIn(500);
	}
	ratioVideo()
});
$(document).on('click', '.modal-fullscreen .btn-close', function () {
	$('.modal-fullscreen').fadeOut(500);
	setTimeout(function(){ $('.modal-fullscreen').remove()},600);
	$('body').removeClass('lock');
});
function ratioVideo() {
	var viwHeight = $(window).height();
	if(window.innerHeight < window.innerWidth){
		$('.modal-fullscreen .container').css('max-width', viwHeight+100);
	}

	var vidWidth = $('#video').width(),
					vidHeight = vidWidth*9.55/16
	$('#video').css('height', vidHeight);
}

$('.btn-img-view').on('click', function (e) {
	e.preventDefault();
	var imgUrl = $(this).attr('data-image');
	if ($(this).hasClass('image-popup')) {
		$('body').addClass('lock');
		$('.footer').after('<div class="modal-fullscreen" style="display: block;"><div class="container"><div class="image-viewer"><div class="btn-close">&#10006;</div><img src="'+imgUrl+'" alt=""></div></div></div>');
		$('.modal-fullscreen').fadeIn(500);
	}
});

var bgSection = $('[data-color]');
if (typeof bgSection !== 'undefined' && bgSection !== false) {
	$('[data-color]').each( function () {
		var data = $(this).attr('data-color');
		$(this).css('background', data)
	});
}

var bgImg = $('[data-bgim]');
if (typeof bgImg !== 'undefined' && bgImg !== false) {
	$('[data-bgim]').each( function () {
		var data = $(this).attr('data-bgim');
		$(this).css('background-image', 'url('+data+')')
	});
}

var videsc = $('[data-videsc]');
if (typeof videsc !== 'undefined' && videsc !== false) {
	videsc.each( function () {
		var desc =  $(this).attr('data-videsc');
		$(this).parent().prepend('<p>'+desc+'</p>');
	});
}

function galleryPopup() {
	var pt = $('.image-popup.js-this')
	if (pt.length !== 0) {
		pt.each(function () {
			var src =  $(this).find('img').attr('src');
			$(this).attr('data-image', src);
		});
	}
}

function clamp() {
	var clamp = $('[data-clamp]');
	if (typeof clamp !== 'undefined' && clamp !== false) {
		clamp.each( function () {
			var clampNumb =  $(this).attr('data-clamp');
			$(this).clamp({clamp:clampNumb});
		});
	}
}

function pagination() {
	var pageChild = ($('.pagination-01').attr('data-show') !== undefined) ? $('.pagination-01').attr('data-show') : 3,
	pageSlim = ($(window).width() > 850) ? 1 : 0,
	cardNews = function () {
		cardNews = $('.pagination-01 .card-style-01'),
		title = $('#category').parent().parent().find('.dynamic-title-js');
		cardNews.each( function (i, title) {
			$(this).attr('data-category', $(this).find('.label-category').html().toLowerCase());
		});
		if ($('#category').val() === 'articles') {
			title.html('Semua Artikel');
			cardNews = $('.pagination-01 .card-style-01[data-category="articles"]');
		}else if ($('#category').val() === 'news') {
			title.html('Semua Berita');
			cardNews = $('.pagination-01 .card-style-01[data-category="news"]');
		}else if ($('#category').val() === 'tips') {
			title.html('Semua Tips');
			cardNews = $('.pagination-01 .card-style-01[data-category="tips"]');
		} else {
			title.html('Semua Kategori');
			cardNews = $('.pagination-01 .card-style-01');
		}
		return cardNews;
	}(),
	sources = function () {
		var result = [];
		for (var i = 0; i < cardNews.length; i++) {
			result.push(i);
		}
		return result;
	}();
	$('#pagination-01').pagination({
		dataSource: sources,
		pageSize: pageChild,
		pageRange: pageSlim,
		callback: function(response, pagination) {
			var dataHtml = '';
			$.each(response, function (index, item) {
					dataHtml += cardNews[item].outerHTML;
			});
			dataHtml += '';
			clamp();
			$('#pagination-01').prev().html(dataHtml);
		}
	});
}

$('#category').on('change', function() {
	$('#data-container').load(window.location.href+' #data-container > *', function () {
		pagination();
		clamp();
	});
})

if ($('#pagination-01').length !== 0) {
	pagination();
}

if ($('.marquee > .track').length !== 0) {
	$('.marquee').append($('.marquee > .track:first-child()').clone()).html();
	// $('.client-logo-wrap').slick({})
}

if (typeof $('.review') !== 'undefined' && $('.review') !== false) {
	if ($('.review').hasClass('no-loop')) {
		var loop = false;
	} else {
		var loop = true;
	}

	if ($('.review').hasClass('no-arrow')) {
		var arrow = false;
	} else {
		var arrow = true;
	}

	if ($('.review').hasClass('blog')) {
		var centerMode = false;
	} else {
		var centerMode = true;
	}

	$('.review').slick({
		centerMode: centerMode,
		centerPadding: '22%',
		slidesToShow: 1,
		autoplay: true,
		autoplaySpeed: 5500,
		dots: true,
		arrows: arrow,
		infinite: loop,
		responsive: [
				{
						breakpoint: 1700,
						settings: {
							centerPadding: '15%',
						}
				},
				{
						breakpoint: 1400,
						settings: {
							centerPadding: '10%',
						}
				},
				{
					breakpoint: 1200,
					settings: {
						centerPadding: '0',
					}
				},
				{
					breakpoint: 850,
					settings: {
						centerPadding: '0',
						arrows: false,
						adaptiveHeight: true
					}
				}
		]
	});
}

if (typeof $('.nasp-slider') !== 'undefined' && $('.nasp-slider') !== false) {
	$('.nasp-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		dots: true,
		arrows: false,
		mobileFirst: true,
		responsive: [
			{
				breakpoint: 850,
				settings: 'unslick'
			}
		]
	});
}

if (typeof $('.promo-slider') !== 'undefined' && $('.promo-slider') !== false) {
	$('.promo-slider').slick({
		slidesToShow: 2,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 3500,
		dots: false,
		arrows: false,
		responsive: [
			{
				breakpoint: 850,
				settings: {
					slidesToShow: 1
			}
			}
		]
	});
}
// END COMPONENT